package ictgradschool.industry.lab11.ex03;

import javax.swing.*;
import java.awt.*;

/**
 * A JPanel that draws some houses using a Graphics object.
 *
 * Complete this class. No hints this time :)
 */
public class ExerciseThreePanel extends JPanel {

    /** All outlines should be drawn this color. */
    private static final Color OUTLINE_COLOR = Color.black;

    /** The main "square" of the house should be drawn this color. */
    private static final Color MAIN_COLOR = new Color(255, 229, 204);

    /** The door should be drawn this color. */
    private static final Color DOOR_COLOR = new Color(150, 70, 20);

    /** The windows should be drawn this color. */
    private static final Color WINDOW_COLOR = new Color(255, 255, 153);

    /** The roof should be drawn this color. */
    private static final Color ROOF_COLOR = new Color(255, 153, 51);

    /** The chimney should be drawn this color. */
    private static final Color CHIMNEY_COLOR = new Color(153, 0, 0);

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseThreePanel() {
        setBackground(Color.white);
    }

    /**
     * Draws eight houses, using the method that you implement for this exercise.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawHouse(g, 125, 177, 3);
        drawHouse(g, 199, 193, 7);
        drawHouse(g, 292, 55, 5);
        drawHouse(g, 29, 110, 8);
        drawHouse(g, 379, 386, 7);
        drawHouse(g, 127, 350, 12);
        drawHouse(g, 289, 28, 2);
        drawHouse(g, 300, 150, 16);
    }

    /**
     * Draws a single house, with its top-left at the given coordinates, and with the given size multiplier.
     *
     * @param g the {@link Graphics} object to use for drawing
     * @param left the x coordinate of the house's left side
     * @param top the y coordinate of the top of the house's roof
     * @param size the size multipler. If 1, the house should be drawn as shown in the grid in the lab handout.
     */
    private void drawHouse(Graphics g, int left, int top, int size) {

        // Draw a house, as shown in the lab handout.

        // Polygons
        int[] xRoof = {0, 5, 10};
        int[] yRoof = {5, 0, 5};
        for (int i = 0; i < xRoof.length; i++) xRoof[i] = left+size*xRoof[i];
        for (int i = 0; i < yRoof.length; i++) yRoof[i] = top+size*yRoof[i];

        int[] xChimney = {7, 8, 8, 7};
        int[] yChimney = {1, 1, 3, 2};
        for (int i = 0; i < xChimney.length; i++) xChimney[i] = left+size*xChimney[i];
        for (int i = 0; i < yChimney.length; i++) yChimney[i] = top+size*yChimney[i];

        // Rectangles
        int yHouse = top + 5*size;
        int wHouse = 10*size;
        int hHouse = 7*size;

        int xDoor = left + 4*size;
        int yDoor = top + 8*size;
        int wDoor = 2*size;
        int hDoor = 4*size;

        int xWindow1 = left + size;
        int xWindow2 = left + 7*size;
        int yWindow = top + 7*size;
        int lWindow = 2*size;

        // Lines
        int xVertLine1 = left + 2*size;
        int xVertLine2 = left + 8*size;
        int y1VertLine = top + 7*size;
        int y2VertLine = top + 9*size;

        int x1HorzLine1 = left + size;
        int x2HorzLine1 = left+size*3;
        int x1HorzLine2 = left+size*7;
        int x2HorzLine2 = left+size*9;
        int yHorzLine = top+size*8;

        g.setColor(ROOF_COLOR);
        g.fillPolygon(xRoof, yRoof,3);
        g.setColor(OUTLINE_COLOR);
        g.drawPolygon(xRoof, yRoof,3);

        g.setColor(CHIMNEY_COLOR);
        g.fillPolygon(xChimney, yChimney, 4);
        g.setColor(OUTLINE_COLOR);
        g.drawPolygon(xChimney, yChimney, 4);

        g.setColor(MAIN_COLOR);
        g.fillRect(left, yHouse, wHouse, hHouse);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(left, yHouse, wHouse, hHouse);

        g.setColor(DOOR_COLOR);
        g.fillRect(xDoor, yDoor, wDoor, hDoor);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(xDoor, yDoor, wDoor, hDoor);

        g.setColor(WINDOW_COLOR);
        g.fillRect(xWindow1, yWindow, lWindow, lWindow);
        g.fillRect(xWindow2, yWindow, lWindow, lWindow);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(xWindow1, yWindow, lWindow, lWindow);
        g.drawRect(xWindow2, yWindow, lWindow, lWindow);
        g.drawLine(xVertLine1, y1VertLine, xVertLine1, y2VertLine);
        g.drawLine(xVertLine2, y1VertLine, xVertLine2, y2VertLine);
        g.drawLine(x1HorzLine1, yHorzLine, x2HorzLine1, yHorzLine);
        g.drawLine(x1HorzLine2, yHorzLine, x2HorzLine2, yHorzLine);
    }
}
package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    private JTextField field1;
    private JTextField field2;
    private JButton addButton;
    private JButton subtractButton;
    private JTextField resultField;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        field1 = new JTextField(10);
        field2 = new JTextField(10);
        addButton = new JButton("Add");
        subtractButton = new JButton("Subtract");
        JLabel resultLabel = new JLabel("Result:");
        resultField = new JTextField(20);

        this.add(field1);
        this.add(field2);
        this.add(addButton);
        this.add(subtractButton);
        this.add(resultLabel);
        this.add(resultField);

        addButton.addActionListener(this);
        subtractButton.addActionListener(this);
    }

    public void actionPerformed(ActionEvent event) {
        Double a = Double.parseDouble(field1.getText());
        Double b = Double.parseDouble(field2.getText());

        if (event.getSource() == addButton) {
            resultField.setText(String.valueOf(roundTo2DecimalPlaces(a+b)));
        }
        else if (event.getSource() == subtractButton) {
            resultField.setText(String.valueOf(roundTo2DecimalPlaces(a-b)));
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}
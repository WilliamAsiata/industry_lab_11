package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private ArrayList<Balloon> balloons;
    private Timer balloonTimer;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloons = new ArrayList<>();
        this.balloons.add(new Balloon(30, 60));

        this.addKeyListener(this);
        this.balloonTimer = new Timer(200, this);
    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        for (Balloon balloon : balloons) {
            balloon.move(getWidth(), getHeight());
        }

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Balloon balloon : balloons) {
            balloon.draw(g);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_UP){
            for (Balloon balloon : balloons) {
                balloon.setDirection(Direction.Up);
            }
            if (!balloonTimer.isRunning()) balloonTimer.start();
        }
        else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            for (Balloon balloon : balloons) {
                balloon.setDirection(Direction.Down);
            }
            if (!balloonTimer.isRunning()) balloonTimer.start();
        }
        else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            for (Balloon balloon : balloons) {
                balloon.setDirection(Direction.Left);
            }
            if (!balloonTimer.isRunning()) balloonTimer.start();
        }
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            for (Balloon balloon : balloons) {
                balloon.setDirection(Direction.Right);
            }
            if (!balloonTimer.isRunning()) balloonTimer.start();
        }
        else if (e.getKeyCode() == KeyEvent.VK_S) balloonTimer.stop();
        else if (e.getKeyCode() == KeyEvent.VK_R) this.balloons.add(new Balloon(30, 60));
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
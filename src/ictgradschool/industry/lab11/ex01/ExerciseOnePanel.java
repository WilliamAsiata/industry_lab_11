package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // Declare JTextFields and JButtons as instance variables here.
    private JTextField heightField;
    private JTextField weightField;
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeightButton;
    private JTextField userBMIField;
    private JTextField maximumWeightField;
    private JTextField minimumWeightField;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        heightField = new JTextField(10);
        weightField = new JTextField(10);
        calculateBMIButton = new JButton("Calculate BMI");
        calculateHealthyWeightButton = new JButton("Calculate Healthy Weight");
        userBMIField = new JTextField(10);
        maximumWeightField = new JTextField(10);
        minimumWeightField = new JTextField(10);

        // Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightLabel = new JLabel("Height in metres:");
        JLabel weightLabel = new JLabel("Weight in kilograms:");
        JLabel userBMILabel = new JLabel("Your Body Mass Index (BMI) is:");
        JLabel maxWeightLabel = new JLabel("Maximum Healthy Weight for your Height:");
        JLabel minWeightLabel = new JLabel("Minimum Healthy Weight for your Height:");

        // Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightLabel);
        this.add(heightField);
        this.add(weightLabel);
        this.add(weightField);
        this.add(calculateBMIButton);
        this.add(userBMILabel);
        this.add(userBMIField);
        this.add(calculateHealthyWeightButton);
        this.add(maxWeightLabel);
        this.add(maximumWeightField);
        this.add(minWeightLabel);
        this.add(minimumWeightField);

        // Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHealthyWeightButton.addActionListener(this);
    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be displayed in the text box.
        if (event.getSource() == calculateBMIButton) {
            Double w = Double.parseDouble(weightField.getText());
            Double h = Double.parseDouble(heightField.getText());
            userBMIField.setText(String.valueOf(roundTo2DecimalPlaces(w/(h*h))));
        }
        else if (event.getSource() == calculateHealthyWeightButton) {
            Double h = Double.parseDouble(heightField.getText());
            maximumWeightField.setText(String.valueOf(roundTo2DecimalPlaces(24.9*h*h)));
            minimumWeightField.setText(String.valueOf(roundTo2DecimalPlaces(19*h*h)));
        }
    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}